CPP=g++ -std=c++14 -Wall -Wextra -pedantic -Werror -Wfatal-errors -Ofast

RM=rm -f

.PHONY: default

default: all

include Objs.mk

.PHONY: all clean objs

all: $(EXE)

clean:
	$(RM) $(EXE) $(OBJS)

objs: $(OBJS)

$(EXE): $(OBJS)
	$(CPP) -o $(EXE) $(OBJS) $(LIBS)

.SUFFIXES:
.SUFFIXES: .cpp .o

.cpp.o:
	$(CPP) -c -o $@ $<

main.o: main.cpp
