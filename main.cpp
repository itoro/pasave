#include <cstring>

#include <iostream>
#include <memory>
#include <sstream>

#include <pulse/introspect.h>
#include <pulse/mainloop.h>

class esc {
public:
  esc(const char *str) : str(str) {}
  const char *str;
};

static const char *const escaping = " !\"$&\'()*;<=>\?[\\]^`|";

static const size_t escapinglen(strlen(escaping));

static auto needescapechar(unsigned char c) {
  if (c < ' ') {
    return true;
  } else {
    size_t l (0);
    size_t r (escapinglen - 1);
    while (l <= r) {
      const auto m = (l + r) / 2;
      const auto d = c - escaping[m];
      if (d < 0) {
        r = m - 1;
      } else if (d == 0) {
        return true;
      } else {
        l = m + 1;
      }
    }
    return false;
  }
}

static auto needescapestring(const char *str) {
  if (str[0] == 0) {
    return true;
  } else {
    for (size_t i (0); str[i] != 0; i++) {
      if (needescapechar(str[i])) {
        return true;
      }
    }
    return false;
  }
}

static auto &operator<< (std::ostream &os, const esc &es) {
  const auto s = es.str;
  if (needescapestring(s)) {
    os << '\'';
    for (size_t i (0); s[i] != 0; i++) {
      const auto c = s[i];
      if (c == '\'') {
        os << "\'\\\'\'";
      } else {
        os << c;
      }
    }
    os << '\'';
  } else {
    os << s;
  }
  return os;
}

class mainloop {
public:
  mainloop() : _ptr(pa_mainloop_new()) {
    if (this->_ptr.get() == NULL) {
      throw "can not create mainloop";
    }
  }
  pa_mainloop *get() const {
    return this->_ptr.get();
  }
private:
  class mainloop_deleter {
  public:
    void operator() (pa_mainloop *ptr) {
      pa_mainloop_free(ptr);
    }
  };
  std::unique_ptr<pa_mainloop, mainloop_deleter> _ptr;
};

class context {
public:
  context(mainloop &ml) {
    const auto api = pa_mainloop_get_api(ml.get());
    if (api == NULL) {
      throw "can not get mainloop api";
    }
    this->_ptr.reset(pa_context_new(api, "pasave"));
    if (!this->_ptr) {
      throw "can not create context";
    }
  }
  pa_context *get() const {
    return this->_ptr.get();
  }
private:
  class context_deleter {
  public:
    void operator() (pa_context *ptr) {
      pa_context_unref(ptr);
    }
  };
  std::unique_ptr<pa_context, context_deleter> _ptr;
};

enum step_t {
  CONNECT,
  CARDS,
  SERVER,
  SINKS,
  SOURCES,
  QUIT
};

struct state {
  state(mainloop &ml, context &ctx)
    : step(CONNECT)
    , ml(ml)
    , ctx(ctx)
    , op(NULL) {}
  step_t step;
  mainloop &ml;
  context &ctx;
  pa_operation *op;
  std::string err;
};

static auto &operator<< (std::ostream &os, step_t s) {
  switch (s) {
  case CONNECT:
    os << "CONNECT";
    break;
  case CARDS:
    os << "CARDS";
    break;
  case SERVER:
    os << "SERVER";
    break;
  case SINKS:
    os << "SINKS";
    break;
  case SOURCES:
    os << "SOURCES";
    break;
  case QUIT:
    os << "QUIT";
    break;
  }
  return os;
}

static auto &operator<< (std::ostream &os, pa_context_state_t s) {
  switch (s) {
  case PA_CONTEXT_UNCONNECTED:
    os << "PA_CONTEXT_UNCONNECTED";
    break;
  case PA_CONTEXT_CONNECTING:
    os << "PA_CONTEXT_CONNECTING";
    break;
  case PA_CONTEXT_AUTHORIZING:
    os << "PA_CONTEXT_AUTHORIZING";
    break;
  case PA_CONTEXT_SETTING_NAME:
    os << "PA_CONTEXT_SETTING_NAME";
    break;
  case PA_CONTEXT_READY:
    os << "PA_CONTEXT_READY";
    break;
  case PA_CONTEXT_FAILED:
    os << "PA_CONTEXT_FAILED";
    break;
  case PA_CONTEXT_TERMINATED:
    os << "PA_CONTEXT_TERMINATED";
    break;
  }
  return os;
}

static void next(state &s);

static auto &operator<< (std::ostream &os, const pa_cvolume &vol) {
  const auto avg (pa_cvolume_avg(&vol));
  const auto p (100.0 * (double) avg / (double) PA_VOLUME_NORM);
  auto ip ((int) (p + (p < 0.0 ? -0.5 : 0.5)));
  ip = ip < 0 ? 0 : (ip > 100 ? 100 : ip);
  return os << ip << '%';
}

static auto sink_info_cb
(pa_context *c, const pa_sink_info *i, int eol, void *userdata) {
  (void) c;
  auto &s = *(state *) userdata;
  if (eol != 0) {
    next(s);
  } else {
    if (i->active_port != NULL) {
      std::cout
        << "pactl set-sink-port "
        << esc(i->name) << ' '
        << esc(i->active_port->name)
        << std::endl;
    }
    std::cout
      << "pactl set-sink-mute "
      << esc(i->name) << ' '
      << (i->mute != 0 ? 1 : 0)
      << std::endl;
    std::cout
      << "pactl set-sink-volume "
      << esc(i->name) << ' '
      << i->volume
      << std::endl;
  }
}

static auto source_info_cb
(pa_context *c, const pa_source_info *i, int eol, void *userdata) {
  (void) c;
  auto &s = *(state *) userdata;
  if (eol != 0) {
    next(s);
  } else {
    if (i->active_port != NULL) {
      std::cout
        << "pactl set-source-port "
        << esc(i->name) << ' '
        << esc(i->active_port->name)
        << std::endl;
    }
    std::cout
      << "pactl set-source-mute "
      << esc(i->name) << ' '
      << (i->mute != 0 ? 1 : 0)
      << std::endl;
    std::cout
      << "pactl set-source-volume "
      << esc(i->name) << ' '
      << i->volume
      << std::endl;
  }
}

static auto server_info_cb
(pa_context *c, const pa_server_info *i, void *userdata) {
  (void) c;
  auto &s = *(state *) userdata;
  std::cout
    << "pactl set-default-sink "
    << esc(i->default_sink_name)
    << std::endl;
  std::cout
    << "pactl set-default-source "
    << esc(i->default_source_name)
    << std::endl;
  next(s);
}

static auto card_info_cb
(pa_context *c, const pa_card_info *i, int eol, void *userdata) {
  (void) c;
  auto &s = *(state *) userdata;
  if (eol != 0) {
    next(s);
  } else {
    if (i->active_profile2 != NULL) {
      std::cout
        << "pactl set-card-profile "
        << esc(i->name) << ' '
        << esc(i->active_profile2->name)
        << std::endl;
    }
  }
}

template <class F>
static auto request(state &s, step_t step, const char *err, F f) {
  s.step = step;
  s.op = f(s.ctx.get(), &s);
  if (s.op == NULL) {
    s.err = err;
    pa_mainloop_quit(s.ml.get(), 1);
  }
}

static void next(state &s) {
  if (s.op != NULL) {
    pa_operation_unref(s.op);
    s.op = NULL;
  }
  switch (s.step) {
  case CONNECT:
    request
      (s,
       CARDS,
       "can not get card info list",
       [] (pa_context *ctx, void *s) {
         return pa_context_get_card_info_list(ctx, card_info_cb, s);
       });
    break;
  case CARDS:
    request
      (s,
       SERVER,
       "can not get server info",
       [](pa_context *ctx, void *s) {
         return pa_context_get_server_info(ctx, server_info_cb, s);
       });
    break;
  case SERVER:
    request
      (s,
       SINKS,
       "can not get sink info list",
       [](pa_context *ctx, void *s) {
         return pa_context_get_sink_info_list(ctx, sink_info_cb, s);
       });
    break;
  case SINKS:
    request
      (s,
       SOURCES,
       "can not get source info list",
       [](pa_context *ctx, void *s) {
         return
           pa_context_get_source_info_list(ctx, source_info_cb, s);
       });
    break;
  case SOURCES:
    s.step = QUIT;
    pa_context_disconnect(s.ctx.get());
    break;
  case QUIT:
    break;
  }
}

static auto context_notify_cb(pa_context *c, void *userdata) {
  bool h (false);
  auto &s = *(state *) userdata;
  const auto st = pa_context_get_state(c);
  switch (st) {
  case PA_CONTEXT_UNCONNECTED:
    break;
  case PA_CONTEXT_CONNECTING:
    switch (s.step) {
    case CONNECT:
      h = true;
      break;
    case CARDS:
      break;
    case SERVER:
      break;
    case SINKS:
      break;
    case SOURCES:
      break;
    case QUIT:
      break;
    }
    break;
  case PA_CONTEXT_AUTHORIZING:
    switch (s.step) {
    case CONNECT:
      h = true;
      break;
    case CARDS:
      break;
    case SERVER:
      break;
    case SINKS:
      break;
    case SOURCES:
      break;
    case QUIT:
      break;
    }
    break;
  case PA_CONTEXT_SETTING_NAME:
    switch (s.step) {
    case CONNECT:
      h = true;
      break;
    case CARDS:
      break;
    case SERVER:
      break;
    case SINKS:
      break;
    case SOURCES:
      break;
    case QUIT:
      break;
    }
    break;
  case PA_CONTEXT_READY:
    switch (s.step) {
    case CONNECT:
      next(s);
      h = true;
      break;
    case CARDS:
      break;
    case SERVER:
      break;
    case SINKS:
      break;
    case SOURCES:
      break;
    case QUIT:
      break;
    }
    break;
  case PA_CONTEXT_FAILED:
    break;
  case PA_CONTEXT_TERMINATED:
    switch (s.step) {
    case CONNECT:
      break;
    case CARDS:
      break;
    case SERVER:
      break;
    case SINKS:
      break;
    case SOURCES:
      break;
    case QUIT:
      pa_mainloop_quit(s.ml.get(), 0);
      h = true;
      break;
    }
    break;
  }
  if (!h) {
    std::ostringstream oss ("unexpected case ");
    oss << st << ' ' << s.step << std::endl;
    s.err = oss.str();
    pa_mainloop_quit(s.ml.get(), 1);
  }
}

static auto process() {
  std::cout << "#!/bin/sh" << std::endl << std::endl;
  mainloop ml;
  context ctx (ml);
  state st (ml, ctx);
  pa_context_set_state_callback(ctx.get(), context_notify_cb, &st);
  st.step = CONNECT;
  if
    (pa_context_connect
     (ctx.get(), NULL, PA_CONTEXT_NOFLAGS, NULL) < 0) {
    throw "can not connect";
  }
  int retval;
  if (pa_mainloop_run(ml.get(), &retval) < 0) {
    throw "can not run mainloop";
  }
  if (retval != 0) {
    throw st.err;
  }
}

int main() {
  try {
    process();
  } catch (const char *str) {
    std::cerr << str << std::endl;
    return 1;
  } catch (const std::string &str) {
    std::cerr << str << std::endl;
    return 1;
  } catch (...) {
    std::cerr << "unknown error" << std::endl;
    return 2;
  }
}
